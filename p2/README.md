# LIS 4369 Extensible Enterprise Solutions

## Jackson Pence

### Project 2 Requirements:
1. Use Assignment 5 screenshots and R Manual to backward-engineer the 
following requirements:
2. Resources:
    a. R Manual: https://cran.r-project.org/doc/manuals/r-release/R-
lang.pdf
    b. R for Data Science: https://r4ds.had.co.nz/
3. Use Motor Trend Car Road Tests data:
    a. Research the data! https://stat.ethz.ch/R-manual/R-
    devel/library/datasets/html/mtcars.html
    b. url = 
    http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv
    Note: Use variable mtcars to read file into. (See Assignment 5 for 
    reading .csv files.)
###################################################

*** Report/Command Development Screenshots ***

![Project 2](p2ss1.png)
![Project 2](p2ss2.png)
![Project 2](p2ss2.png)


*** R Studio Screenshots ***

*** 4 Panel *** 

![Panels](4panel.png)


*** R Statistics Output Screenshots ***

![R Studio](r1.png)
![R Studio](r2.png)
![R Studio](r3.png)
![R Studio](r4.png)
![R Studio](r5.png)
![R Studio](r6.png)
![R Studio](r7.png)

*** Graph Output File Screenshots ***

![Graph 1](plotfile1.png)
![Graph 2](plotfile2.png)

