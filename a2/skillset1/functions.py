

def get_requirements():

    print("Square feet to Acres:\n" )
    print("Developer: Jackson Pence")
    print("Program Requirements: \n"
        + "1. Research: number of square feet to acre of land.\n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round counversion to two decimal places.\n")

def calc_acres():

    print("Input: \n")
    feet= int(input("Enter square feet: "))

    acres = feet / 43560

    acres= (round(acres, 2))
    format_feet = "{:.2f}".format(feet)
    format_acres = "{:.2f}".format(acres)
    

    print("Output: \n")
    print(format_feet + " square feet = " + format_acres + " acres.")

    
