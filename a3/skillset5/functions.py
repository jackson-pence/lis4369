def get_requirements():

    print("Python Selection Structures")
    print("\nProgram Requirements:\n"
    +"1. Use Python selection structure\n"
    +"2. Prompt uesr for two numbers and a suitable operatior.\n"
    +"3. Test for correct numeric operator.\n"
    +"4. Replicate example display.\n")


def get_input():

    print("Python calculator")
    num1 = input("Enter number 1: ")
    num2 = input("Enter number 2: ")

    print("\nSuitable Operators: +,-,*,/,//(integer division, %(modulo operator), **(power)")
    operator = input("Enter operator: ")

    if num1.isnumeric() == True and num2.isnumeric() == True:

        n1 = float(num1)
        n2 = float(num2)
        operate(n1,n2,operator)
    else:
        print("Enter numbers not letters")


def operate(num1, num2, operator):

    if operator == "+":
        num3=num1+num2
        print(num3)
    elif operator == "-":

        num3=num1-num2
        print(num3)
    elif operator == "*":
        num3=num1*num2
        print(num3)
    elif operator == "/":
        if(num2==0):
            print("Cannot divide by zero")
        else:
            num3=num1/num2
            print(num3)
    elif operator == "//":
        if(num2==0):
            print("Cannot divide by zero")
        else:
            num3 = num1 // num2
            print(num3)
    elif operator == "%":
        if(num2==0):
            print("Cannot divide by zero")
        else:
            num3=num1%num2
            print(num3)
    elif operator == "**":
        num3=num1**num2
        print(num3)
    else:
        print("Illegal operator!")


def main():
    get_requirements()
    get_input()


if __name__ == "__main__":
    main()
    
    