
def print_requirements():

    print("Miles per gallon")
    print("Developer: Jackson Pence")
    print("Program Requirements:\n"
        + "1. Calculate MPG.\n"
        + "2. Must use float data type for user input and calculation\n"
        + "3. Format and round conversion to two decimal places\n")


def calc_miles():

    print("Input: ")

    miles = float(input("Enter miles driven: "))
    gallons = float(input("Enter gallons of fuel used: "))

    mpg = miles/gallons

    print ("\nOutput: ")

    print("{0:,.2f} {1} {2:2,.2f} {3} {4:,.2f}".format(miles,"miles driven and", gallons,"gallons used = ", mpg, "mpg.", ))


def main():
    print_requirements()
    calc_miles()

    if __name__ == "__main__":
        main()