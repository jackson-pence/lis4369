def get_requirements():

    print("Python Sets - like mathematical sets!")
    print("\nProgram Requirements: \n"
    +"1. Sets: mutable, heterogeneous, undordered sequence of elements, but cannot hold duplicate values.\n"
    +"2. Sets are mutable/changeable - can insert, update, delete.\n"
    +"3. While sets are mutable/changeable, they cnanot contain other mutable items like lists, sets, or dictionaries. \n"
    +"\t which means, elements in the set must be immutable.\n"
    +"4. Also, since sets are unordered, cannot use indexing to access, update, or delete elements.\n"
    +"5. Two methods to create sets: \n"
    +"\ta. Create set using curly brackets {set}:my_set = {1,3.14,2.0,four,five}.\n"
    +"\tb. Create set using set() function: my_set=set(<iterable>).\n"
    +"\t Examples: \n"
    +"\tmy_set1 = set([1,3.14,2.0,four,five]) #set with list.\n"
    +"\tmy_set2 = set((1,3.14,2.0,four,five)) #set with tuple.\n"
    +"Note: an iterable is any object which can be iterated over - lists, tuples, strings, etc.\n"
    +"6. Create a program that mirrors the following IPO format.\n")


def using_sets():
    print("Input: Hard coded - no user input. See three examples above.")
    print("***Note***, All three sets below print as \*sets\* regardless of how there were created!\n")

    my_set = {1,3.14,2.0,'four','Five'}
    print("Print my_set created using curly brackets: ")
    print(my_set)
    print("\nPrint type of my_set: ")
    print(type(my_set))

    my_set1 = set([1,3.14,'four','Five'])
    print("\nPrint my_set1 created using set() function with list: ")
    print(my_set1)
    print("\nPrint type of my_set1")
    print(type(my_set1))

    my_set2 = set((1, 3.14, 2.0, 'four','Five'))
    print("\nPrint my_set2 created using set() function with tuple")
    print(my_set2)
    print("\nPrint type of my_set2: ")
    print(type(my_set2))

    print("\nLength of my_set")
    print(len(my_set))

    print("\nDiscard 'four'")
    my_set.discard('four')
    print(my_set)

    print("\nRemove 'Five'")
    my_set.remove('Five')
    print(my_set)

    print("\nLength of my_set: ")
    print(len(my_set))

    print("\nAdd elelemt to set (4) using add() function: ")
    my_set.add(4)
    print(my_set)

    print("\nLength of my_set: ")
    print(len(my_set))

    print("\nDisplay minimum number: ")
    print(min(my_set))

    print("\nDisplay maximum number: ")
    print(max(my_set))

    print("\nDisplay sum of numbers: ")
    print(sum(my_set))

    print("\nDelete all elements: ")
    my_set.clear()
    print(my_set)

    print("\nLength of my_set: ")
    print(len(my_set))


import functions as f 
    
def main():
        get_requirements()
        using_sets()

if __name__ == '__main__':
        main()