
# LIS 4369 Extensible Enterprise Solutions

## Jackson Pence

### Project 1 Requirements:

#### Development 

1. Requirements:
    a. Code and run **demo.py. (Note: *be sure* necessary packages are installed!)
    **b. Use it to backward-engineer the screenshots below it.**
    c. Update **conda** and install necesary Python packages. Invoke the commands below.
        1) conda update conda
        2) conda update --all
        3) pip install pandas-datareader
    2. Be sure to test your program using both IDLE and Visual Studio Code

#### README.md file should include the following items:

* **1. Assignment requirements, as per A1**
* 2. Screenshots as per examples below, including **Jupyter Notebook screenshots and graphs**
* 3. Upload A3.ipynb file and create link in README.md 
    Note: *Before* uploading .ipynb file, *be sure* to do the following actions from the Kernal menu:
        a. Restart & clear output
        b. Restart & run all 

#### Deliverables:
1. Provide **Bitbucket** read access to **lis4349** repo, include links to the repos you created in **README.md** using Markdown sytax
2. FSU's Learning Management System: lis4369 Bitbucket repo

#### Assignment Screenshots:

*Screenshot of main.py application running (IDLE), *: 

![Project 1](p1ss1.png)
![Project 1](p1ss2.png)
![Project 1](p1graph.png)


*Screenshots of P1 Jupyter Notebook*:

![Jupyter Notebook screenshot #1](jpn1.png)
![Jupyter notebook screenshot #2](jpn2.png)
![Jupyter notebook screenshot #3](jpn3.png)
![Jupyter notebook screenshot #4](jpn4.png)

#### Skillsets:

*Screenshot of Skillset #7 application running (IDLE)*: 
![Using Lists](ss7.png)

*Screenshot of Skillset #8 application running (IDLE)*: 

![Using Tuples](ss8.png)

*Screenshot of Skillset #9 application running (IDLE)*: 

![Using Sets](ss91.png)
![Using Sets](ss92.png)






