# LIS 4369 Extensible Enterprise Solutions

## Jackson Pence

### Assignment 4 Requirements:

#### Development 
1. Requirements: 
     a) Code and run demo.py. (Note: *be sure* necessary packages are installed!) 
     Note: If needed, see previous assignment for installing Python packages. 
    b) Then. use demo.py to backward-engineer the screenshots below it. 
    c) When displaying the required graph (see code below), answer the following question: 
    Why is the graph line split? 

2. Be sure to test your program using both IDLE and Visual Studio Code. 

#### README.md file should include the following items:

* Assignment requirements, as per A1
* Screenshots as per examples below, including graph
* Upload A4.ipynb file and create link in README.md 
    Note: *Before* uploading .ipynb file, *be sure* to do the following actions from the Kernal menu:
        a. Restart & clear output
        b. Restart & run all 

#### Deliverables:
1. Provide Bitbucket read-only access to lis4369 repo link.
2. Also, *be sure* the assigment README.md includes screenshots.

#### Assignment Screenshots:

*Screenshots of main.py application running (IDLE), graph pictured at the end*: 
![req](start.png)
![ss1](1.png)
![ss2](2.png)
![ss3](3.png)
![ss4](4.png)
![ss5](5.png)
![graph](graph.png)

### The graph line is split because 3 of the entries do not have an age field (n/a) recorded, thus resulting in a gap



*Screenshots of A4 Jupyter Notebook*:

![Jupyter Notebook screenshot #1](jp1.png)
![Jupyter notebook screenshot #2](jp2.png)
![Jupyter notebook screenshot #3](jp3.png)
![Jupyter notebook screenshot #4](jp4.png)
![Jupyter notebook screenshot #5](jp5.png)
![Jupyter notebook screenshot #6](jp6.png)
![Jupyter notebook screenshot #6](jp7.png)

#### ** Beginning and end of Jupyter Notebook output shown. Output is directly identical to screenshots of main.py running in IDLE above. **


#### Skillsets:

*Screenshot of Skillset #10 application running (IDLE)*: 

![Python Dictionaries](ss10.png)

*Screenshot of Skillset #11 application running (IDLE)*: 

![Pseudo Random Number Generator](ss11.png)

*Screenshot of Skillset #12 application running (IDLE)*: 

![Temperature Conversion](ss12.png)






