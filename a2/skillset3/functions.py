
def get_requirements():
    print("IT/ICT Student Percentage:\n" )
    print("Program Requirements: \n"
        + "1. Find number of IT/ICT students in class.\n"
        + "2. Calculate IT/ICT student percentage.\n"
        + "3. Must use float data type (to faciliate right-alignment).\n"
        + "4. Format, right-align numbers, and round to two decimal places.\n")

def calculate_percent():

    print("Input: ")

    it_students = int(input("Enter number of IT students: "))
    ict_students = int(input("Enter number of ICT students: "))

    total = it_students + ict_students
    it_percent = (it_students/total) 
    ict_percent = (ict_students/total) 

    print ("Output: \n")
    print("{0:17} {1:>5.2f}".format("Total students:",total))
    print("{0:17} {1:>5.2%}".format("IT Students:",it_percent))
    print("{0:17} {1:>5.2%}".format("ICT Students:",ict_percent))