def get_requirements():
    print("Temperature Conversion Program")
    print("\nProgram Requirements: \n"
    +" 1. Program converts user-entered temperatures into Fahrenheit or Celsius scales.\n"
    +" 2. Program continues to prompt for user entry until no longer requested.\n"
    +" 3. Note: upper or lower case letters permitted. Though, incorrect entries are not permitted.\n"
    +" 4. Note: Program does not validate numeric data (optional requirement).\n")



def main():

    get_requirements()
    print("Input: ")

    i = input("\nDo you want to convert a temperature? (y or n): ").lower()

    print("\nOutput: ")
    

    if i == "y":
        while i == "y" or i == "n":
            convert()
            i = input("\nDo you want to convert another temperature? (y or n): ").lower()
            if i == "n":
                break
    elif i == "n":
        print("\nThank you for using our Temperature Conversion Program!")
        exit()
    else:
        print("Enter y or n please.")
        i = input("\nDo you want to convert another temperature? (y or n): ").lower()

        while i == "y" or i == "n":
            convert()
            i = input("\nDo you want to convert another temperature? (y or n): ").lower()
            if i == "n":
                break


        

    print()

    print("Thank you for using our Temperature Conversion Program!")



def convert():

    type = input("Farenheit to Celsius? Type \"f\", or Celsius to Farenheit? Type \"c\": ").lower()
    
    if type == "f":
        num = input("Enter temperature in Farenheit: ")
        if num.isnumeric():
            num = int(num)

            numerator = (num - 32) * 5

            f = round(numerator / 9, 2)

            print("Temperature in Celsius: ", f)
        else:
            print("Please enter a number. ")

    elif type == "c":
        num = input("Enter temperature in Celsius: ")
        if num.isnumeric():
            num = int(num)

            var = round(1.8*num, 1)

            final = var + 32

            print("Temperature in Farenheit: ", final)

        else:
            print("Please enter a number. ")



