def get_requirements():

    print("Painting Estimator")
    print("\nProgram Requirements:\n"
    + "1. Calculate home interior paint cost (w/o) primer).\n"
    + "2. Must use float data types \n"
    + "3. Must use SQFT_PER_GALLON constant (350).\n"
    + "4. Must use iteration structure (aka \"loop\").\n"
    + "5. Format, right-align numbers, and round to two decimal places.\n"
    + "6. Create at least five functions that are called by the program.: \n"
    + "\ta. main(): calls two other functions: get_requirements() and estimate_painting_cost()\n"
    + "\tb. get_requirements(): displays the program requirements.\n"
    + "\tc. estimate_painting_cost(): calculates interior home painting and calls print functions.\n"
    + "\td. print_painting_estimate(): displays painting costs.\n"
    + "\te. print_painting_percentage(): displays painting costs as percentages.\n")


def estimate_painting_cost():

    SQFT_PER_GALLON = 350
    total_sqft = 0.0


    while True:
        print("Input:")
        total_sqft=float(input("Enter total square feet: "))
        price_per_gallon=float(input("Enter price per gallon of paint: "))
        painting_rate=float(input("Enter hourly painting rate per sq ft: "))

        paint_gallons= total_sqft / SQFT_PER_GALLON
        paint_cost= paint_gallons * price_per_gallon
        labor_cost= total_sqft * painting_rate
        total_cost= paint_cost + labor_cost
        paint_percent= paint_cost/total_cost
        labor_percent= labor_cost / total_cost

        print_painting_estimate(total_sqft, SQFT_PER_GALLON, paint_gallons, price_per_gallon, painting_rate)

        print_painting_percentage(paint_cost, paint_percent, labor_cost, labor_percent, total_cost)
        answer=input("Estimate another paint job? (y/n): \n")
        if answer=="n":
            break
    
    print("Thank you for using our Painting Estimator!\n")
    print("Please see our site at http://www.mysite.com\n")









def print_painting_estimate(total_sqft, SQFT_PER_GALLON, paint_gallons, price_per_gallon, painting_rate):

    print("\nOutput:")
    print("{0:20} {1:>9}".format("Item", "Amount"))
    print("{0:20} {1:9,.2f}".format("Total Sq Ft:", total_sqft))
    print("{0:20} {1:9,.2f}".format("Sq Ft per gallon:", SQFT_PER_GALLON))
    print("{0:20} {1:9,.2f}".format("Number of gallons:", paint_gallons))
    print("{0:20} ${1:8,.2f}".format("Paint per gallon:", price_per_gallon))
    print("{0:20} ${1:8,.2f}".format("Labor per sq ft:", painting_rate))

    print()



def print_painting_percentage(paint_cost, paint_percent, labor_cost, labor_percent, total_cost):

    print("{0:8} {1:>9} {2:>13}".format("Cost", "Amount", "Percentage"))
    print("{0:8} {1:8,.2f} {2:13.2%}".format("Paint:", paint_cost, paint_percent))
    print("{0:8} {1:8,.2f} {2:13.2%}".format("Labor:", labor_cost, labor_percent))
    print("{0:8} {1:8,.2f} {2:13.2%}".format("Total:", total_cost, paint_percent + labor_percent))



def main():
    get_requirements()
    estimate_painting_cost()


if __name__ == "__main__":
    main()