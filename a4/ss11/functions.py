def get_requirements():
    print("Pseudo Random Number Generator")
    print("\nProgram Requirements: \n"
    +"1. Get user beginning and ending integer values, and store in two variables.\n"
    +"2. Display 10 pseudo-random numbers between, and including, above values.\n"
    +"3. Must use integer data types.\n"
    +"4. Example 1: Using range() and randint() functions.\n"
    +"5. Example 2: Using a list with range() and shuffle() functions.\n")



def rand_num():
    
    import random 

    print("Input:")
    start=int(input("Enter beginning value: "))
    end = int(input("Enter ending value: "))

    print()
    
    my_sequence= range(6)


    print("Output: ")
    print("Example 1: Using range() and randint() functions.")
    for item in range(10):
        print(random.randint(start,end), sep=", ",end=" ")

    print()

    print("\nExample 2: Using a list with range() and shuffle() functions.")
    my_list=list(range(start,end + 1))

    random.shuffle(my_list)
    for item in my_list:
        print(item, sep=",", end=" ")

    print()


    
def main():
        get_requirements()
        rand_num()
if __name__ == '__main__':
        main()