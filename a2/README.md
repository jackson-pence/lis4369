
# LIS 4369 Extensible Enterprise Solutions

## Jackson Pence

### Assignment 2 Requirements:

*Four Parts:*

1. Backward-engineer (using Python) the following screenshots:
2. The program should be organized with two modules(See Ch. 4):\
     a) functions.py module contains the following functions\
        i) get_requirements()\
        ii) calculate_payroll()\
        iii) print()pay\
    b) main.py module imports the functions.py module, and calls the functions.\
    c) Note: *ALWAYS* run your program from main.py\

3. Be sure to test your program using both IDLE and Visual Studio Code
4. *Be Sure* to carefully review (How to Write Python): https://realpython.com/lessons/what-pep-8-and-why-you-need-it/

#### README.md file should include the following items:

* Assignment requirements, as per A1
* Screenshots as per examples below
* Upload A2.ipynb file and create link in README.md 
    Note: *Before* uploading .ipynb file, *be sure* to do the following actions from the Kernal menu:
        a. Restart & clear output
        b. Restart & run all 

#### Deliverables:
1. Provide Bitbucket read-only access to lis4369 repo link.
2. Also, *be sure* the assigment README.md includes screenshots.

#### Assignment Screenshots:

*Screenshot of main.py application running (IDLE), payroll no overtime*: 

![Payroll no overtime](no_overtime.png)

*Screenshot of main.py application running (IDLE), payroll with overtime*:

![Payroll with overtime](overtime.png)

*Screenshots of A2 Jupyter Notebook*:

![Jupyter Notebook screenshot #1](a2ipynb1.png)
![Jupyter notebook screenshot #2](a2ipynb2.png)
![Jupyter notebook screenshot #3](a2ipynb3.png)

#### Skillsets:

*Screenshot of Skillset #1 application running (IDLE)*: 

![Square feet to acres](skillset1.png)

*Screenshot of Skillset #2 application running (IDLE)*: 

![Miles per gallon](skillset2.png)

*Screenshot of Skillset #3 application running (IDLE)*: 

![IT/ICT student percentage](skillset3.png)






