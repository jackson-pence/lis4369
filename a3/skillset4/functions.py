def get_requirements():

    print("Calorie Percentage")
    print("\nProgram Requirements:\n"
    + "1. Find calories per grams of fat, carbs, and protein.\n"
    + "2. Calculate percentages.\n"
    + "3. Must use float data types.\n"
    + "4. Format, right-align numbers, and round to two decimal places.\n")


def get_user_input():

    fat_grams = 0.0
    carb_grams = 0.0
    protein_grams = 0.0

    print("Input:")
    fat_grams = float(input("Enter total fat grams: "))
    carb_grams = float(input("Enter total carb grams: "))
    protein_grams = float(input("Enter total protein grams: "))

    return fat_grams, carb_grams, protein_grams


def calculate_cals(f_grams, c_grams, p_grams):

    total_calories = 0.0
    percent_fat = 0.0
    percent_carbs = 0.0
    percent_protein = 0.0

    cals_from_fat=f_grams * 9
    cals_from_carbs=c_grams * 4
    cals_from_protein=p_grams * 4
    total_calories=cals_from_fat+cals_from_carbs+cals_from_protein

    percent_fat = cals_from_fat / total_calories
    percent_carbs = cals_from_carbs / total_calories
    percent_protein = cals_from_protein / total_calories

    print("\nOutput:")
    print("{0:8} {1:>10} {2:>13}".format("Type","Calories","Percentage"))
    print("{0:8} {1:10,.2f} {2:13.2%}".format("Fat", cals_from_fat, percent_fat))
    print("{0:8} {1:10,.2f} {2:13.2%}".format("Carbs", cals_from_carbs, percent_carbs))
    print("{0:8} {1:10,.2f} {2:13.2%}".format("Protein", cals_from_protein, percent_protein))

    
def main():

    get_requirements()

    user_input = get_user_input()

    fat, carb, protein = user_input

    calculate_cals(fat, carb, protein)

if __name__ == "__main__":
    main()