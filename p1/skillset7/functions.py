def get_requirements():
    print("Python Lists")
    print("\nProgram Requirements: \n"
    + "1. Lists (Python data structure): mutable, ordered sequence of elements. \n"
    + "2. Lists are mutable/changeable = that is, can insert, update, and delete. \n"
    + "3. Create list - using square bracket [list]: my_list = [cherries,apples,bananas,oranges].\n"
    + "4. Create a program that mirrors the following IPO.\n"
    + "Note: user enters number of requested list elements, dynamically rendered below (number of elements can change each run)\n.")

def user_input():

    num = 0

    print("Input: ")
    num = int(input("Enter number of list elements: "))
    return num
    print()

def using_lists(num):

    my_list= []

    for i in range(num):
        my_element = input("Please enter list element" + str(i+1) + ": ")
        my_list.append(my_element)

    print("\nOutput")
    print("Printing my_list")
    print(my_list)

    elem = input("\nPlease enter list element: ")
    position= int(input("Please enter list index position (must convert to int): "))

    print("\nInsert element into specific position in my_list: ")
    my_list.insert(position, elem)
    print(my_list)

    print("\nCount number of elements in list: ")
    print(len(my_list))

    print("\nSort elements in list alphabetically: ")
    my_list.sort()
    print(my_list)

    print("\nReverse list: ")
    my_list.reverse()
    print(my_list)

    print("\nRemove last element in list: ")
    my_list.pop()
    print(my_list)

    print("\nDelete second element from list by index (note: elements start at index 0): ")
    del my_list[1]
    print(my_list)

    print("\n Delete element from my list by value (cherries)")
    del my_list[1]
    print(my_list)

    print("\n Delete all elements from list: ")
    my_list.clear()
    print(my_list)


import functions as f 
    
def main():
        get_requirements()
        user_input()
        using_lists(num)

if __name__ == '__main__':
        main()