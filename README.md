

# LIS4369 - Extensible Enterprise Solution (Python/R Data Analytics/Visualization)

## Jackson Pence

### LIS4369 Requirements:

*Course Work Links:*

1. [A1.README.md](a1/README.md)
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create *a1_tip_calculator* application
    - Create *a1_tip_calculator* Jupyter Notebook
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2.README.md](a2/README.md)
    - Create payroll_calculator application
    - Create payroll_calculator Jupyter Notebook
    - Create Skillsets 1-3 (Square feet to Acres, Miles per gallon, IT/ICT student percentage)

3. [A3.README.md](a3/README.md)
    - Create paint_calculator applicaton
    - Create paint_calculator Jupyter Notebook
    - Create Skillsets 4-6 (Calories Calculator, Python Calculator, Python Loops)

3. [P1.README.md](p1/README.md)
    - Data analysis using Pandas data reader
    - Graph of year over year comparisons of Exxon Mobil and Yahoo
    - Create Skillsets 7-9 (Using lists, Using tuples, Using sets)

5. [A4.README.md](a4/README.md)
    - Data analysis (2) using Pandas data reader
    - Graph of ages of a database of passengers
    - Create Skillsets 10-12 (Using dictionaries, Random number generator, Temperature conversion program)
    
6. [P2.README.md](p2/README.md)
    - Data analysis of mtcars data set using R 
    - Graph of mpg, weight, and cylinder count
    - Seperation of graph images to pdf output file


