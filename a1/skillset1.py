
print("Square Feet to Acres")

print("Program Requirements:")
print("1. Must use float data type for user input (except, \"Party number\").")
print("2. Must round calculations to two decimal places.")
print("3. Must format currency with dollar sign, and two decimal places")

print("Input: ")
feet = float(input("Enter square feet: "))

acres = (feet/435460.00)

print("Output: ")

print(feet) + "square feet = " + (acres) + "acres")